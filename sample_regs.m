function scores = sample_regs()
  % import Manopt if not yet:
  %   curpath = pwd();
  %   cd('path/to/manopt');
  %   importmanopt
  %   cd(curpath);

  params.M = 6; % Original dimensionality
  params.diversity_level = 0.3;

  params.T = 10;
  params.K = 3;  % Reduced dimensionality
  params.ntr =  10;  % Number of samples (for computing the input covariance)
  params.nte  =  10000;  % Number of samples (for computing the input covariance)
  params.use_cv = false;
  params.regs = [0, logspace(-2, 1, 5), Inf];  % Regularization parameter

  scores = zeros(params.T, length(params.regs));

  % Generate Training/Test Data
  [covMat_t] = tilted_model(params);
  [x_t, xte_t] = generate_x_t(covMat_t, params);
  % Test sample covariance matrices
  for tt = 1:params.T
      cte_t{tt} = xte_t{tt} * xte_t{tt}' / size(xte_t{tt}, 2);
  end


  U = mtpca(x_t, params);
  scores = eval_score(cte_t, U);
  ave_scores = mean(scores, 1);

  fprintf('Data Type: Tilted Gaussians\n');
  fprintf('Original Dimensionality: %g\n', params.M);
  fprintf('Dimensionality After PCA: %g\n', params.K);
  fprintf('Number of Samples: %g\n', params.ntr);
  fprintf('Number of Tasks: %g\n', params.T);
  fprintf('Retained Variance Ratio (Averaged over tasks. Higher, better.):\n');
  for i = 1:length(params.regs)
    fprintf('\tMT-PCA(reg=%0.4f):\t%g\n', params.regs(i), ave_scores(i));
  end
end


function [x_t, xte_t] = generate_x_t(covMat_t, params)
    for t = 1:params.T
        M = size(covMat_t{t}, 1);

        R = chol(covMat_t{t});
        x_t{t} = (randn(params.ntr, M) * R)';
        xte_t{t} = (randn(params.nte, M) * R)';
    end
end

function  [covs, C0] = tilted_model(params)
  M = params.M;
  diag_elements = [1:floor(M/2), 1:(M-floor(M/2))]';

  temp = randn(M, ceil(1.5 * M));
  [U0, ~] = eig(temp * temp'); % random orthogonal matrix
  C0 = U0 * diag(diag_elements) * U0';

  for t = 1:params.T
      R_near_I = eye(M) + params.diversity_level * randn(M);
      Vt = proj2orthgroup(R_near_I);
      covs{t} = Vt * C0 * Vt';
  end

  function PX = proj2orthgroup(X)
      [U, ~, V] = svd(X);
      PX = V * U';
  end
end

