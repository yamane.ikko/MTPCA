% mt_pca.m
% 
% Copyright (c) Florian Yger
% Copyright (c) Ikko Yamane
% 
% This software is released under the MIT License.
% http://opensource.org/licenses/mit-license.php

%% MT_PCA
% Estimates principal subspaces by multitask principal component analysis.
%
% Format: Us = mt_pca(x_t, params)
%
% Input:
%   x_t: Cell array of d-by-n matrices, where d is the number of features and n is the number of instances. Each matrix is the data matrix for each task. **Every data matrix needs to have mean zero.**
%   params.T: Int. The number of tasks.
%   params.use_cv: Boolean. Indicates whether to perform cross-validation for regularization parameter selection. If false, it outputs the cell array of the solutions for all the candidate parameters.
%   params.regs: Float array. Candidates for the regularization parameter.
%   params.ncvfolds: The number of folds in K-fold cross validation.
%   params.K: The dimensionality of the principal subspace to be extracted.
%
% Output: 
%   1) When params.use_cv is true,
%     Us: (params.T)-by-1 cell array of d-by-(params.K) matrices. Each matrix U satisfies U * U = eye(params.K) and spans the principal subspace estimated with the cross-validated regularization paramter.
%   2) When params.use_cv is false,
%     Us: length(params.regs)-by-1 cell array of cell arrays of (params.T)-by-1 cell array of d-by-(params.K) matrices. Each matrix U satisfies U * U = eye(params.K) and spans the principal subspace estimated with one of the candidate regularization parameters in params.regs.
%
function Us = mt_pca(x_t, params)

  if params.use_cv
    Us = estimate_cv(x_t, params);
  else
    for i_reg = 1:length(params.regs)
      U_t = estimate_with_fixed_reg(x_t, params.regs(i_reg), params);
      Us{i_reg} = U_t;
    end; clear i_reg;
  end
end


function [U_t, reg_best] = estimate_cv(x_t, params)
  for t = 1:params.T
      ind_t{t} = floor(randperm(size(x_t{t}, 2)) / params.ncvfolds) + 1;
  end
  cv_score = zeros(params.T, length(params.regs), params.ncvfolds);
  for i_cv = 1:params.ncvfolds
      for t = 1:params.T
          x_t_tr{t} = x_t{t}(:, ind_t{t} ~= i_cv);
          x_t_vld{t} = x_t{t}(:, ind_t{t} == i_cv);
      end

      for i_reg = 1:length(params.regs)
          U_reg{i_reg} = estimate_with_fixed_reg(x_t_tr, params.regs(i_reg), params);
      end

      for t = 1:params.T
          c_t_vld{t} = x_t_vld{t}*x_t_vld{t}' / size(x_t_vld{t}, 2);
      end
      cv_score(:, :, i_cv) = eval_score(c_t_vld, U_reg); % (Task, regs)
  end
  mscore = mean(mean(cv_score, 3), 1);
  [~, i_reg] = max(mscore, [], 2);
  reg_best = params.regs(i_reg);
  U_t = estimate_with_fixed_reg(x_t, reg_best, params);
end


function U_t = estimate_with_fixed_reg(x_t, reg, params)
  x_batch = [x_t{:}];
  c_batch = x_batch * x_batch' / size(x_batch, 2);
  for t = 1:params.T
    c_t{t} = x_t{t} * x_t{t}' / size(x_t{t}, 2);
  end

  if reg == Inf % Single-Common Solution
    [Upca, ~] = eigs(c_batch, params.K);
    for t = 1:params.T
        U_t{t} = Upca;
    end
  elseif reg == 0 % Independent Solutions
    for t = 1:params.T
        [U_t{t}, ~] = eigs(c_t{t}, params.K);
    end
  else % Multi-Task Solutions
    for t = 1:params.T
        U_init{t} = eye(params.M, params.K); %or U_indTasks_t{t} or U_singleTask_t{t}
    end
    U_t = solve_mt_pca(params.K, c_t, reg, U_init);
  end
end

function U_t = solve_mt_pca(K, covs, lambda, Us_init)
  MNsolver=@trustregions;
  opt.verbosity=0;

  warning('off', 'manopt:getHessian:approx');

  T = length(covs);
  M = size(covs{1}, 1);

  tuple = [];
  for t = 1:T
      name_fields{t}=['U' int2str(t)];
      tuple = setfield(tuple, name_fields{t}, grassmannfactory(M, K));
  end

  manifold = productmanifold(tuple);

  X0 = [];
  for t = 1:T
      X0 = setfield(X0, name_fields{t}, Us_init{t});
  end

  problem.M = manifold;
  problem.cost = @cost;
  problem.egrad = @egrad;

  [x, costHist, info, options] = MNsolver(problem, X0, opt);

  for i = 1:T
     U_t{i} = getfield(x, name_fields{i});
  end

  function f = cost(X)
    accu = 0;
    accuReg = 0;

    T=length(fieldnames(X));
    for t = 1:T
        Utmp = getfield(X, name_fields{t});
        accu = accu + trace(Utmp'*covs{t}*Utmp);
        regu = 0;
        for t2 = setdiff([1:T], t)
            Utmp2 = getfield(X, name_fields{t2});
            regu = regu + trace((Utmp*Utmp')*(Utmp2*Utmp2'));
        end
        accuReg = accuReg + regu;
    end
    f = -0.5*accu - (lambda/4)*accuReg;
  end

  function g = egrad(X)
    g = [];
    T = length(fieldnames(X));
    for t = 1:T
        Ulocal = getfield(X, name_fields{t});
        gUtmp = covs{t}*Ulocal;
        for t2 = setdiff([1:T], t)
            tmpU = getfield(X, name_fields{t2});
            gUtmp = gUtmp + lambda*(tmpU*tmpU')*Ulocal;
        end
        g = setfield(g, name_fields{t},-gUtmp);
    end
  end
end

