MTPCA is an implementation of the following work:
```
@InProceedings{pmlr-v63-yamane65,
    title =    {Multitask Principal Component Analysis},
    author =     {Ikko Yamane and Florian Yger and Maxime Berar and Masashi Sugiyama},
    booktitle =    {Proceedings of The 8th Asian Conference on Machine Learning},
    pages =      {302--317},
    year =     {2016},
    editor =     {Robert J. Durrant and Kee-Eung Kim},
    volume =   {63},
    series =     {Proceedings of Machine Learning Research},
    address =      {The University of Waikato, Hamilton, New Zealand},
    month =      {16--18 Nov},
    publisher =    {PMLR},
    pdf =    {http://proceedings.mlr.press/v63/yamane65.pdf},
    url =      {http://proceedings.mlr.press/v63/yamane65.html},
}
```

# Dependencies
- [MATLAB](https://www.mathworks.com/products/matlab.html)
- [Manopt](https://manopt.org/)

# Setup
1. Place the MTPCA package in any directory.
2. Import Manopt (See [Manopt tutorial](https://manopt.org/tutorial.html) for details):
```
cd /path/to/manopt
importmanopt
```

# Usage
- Please have a look at the 'help' documentation:
```
help mtpca
```

# Example
Example scripts can be found in the package:
- sample_regs.m
Sample script running MTPCA with several regularization parameter.
- sample_cv.m
Sample script running MTPCA with cross-validated regularization parameter.

# Contact
yamane@ms.k.u-tokyo.ac.jp

# Copyright & License
Copyright (c) 2016 Ikko Yamane
Copyright (c) 2016 Florian Yger
This software is released under the MIT License, see LICENSE.txt.

