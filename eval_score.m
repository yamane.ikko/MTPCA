function score = eval_score(test_covs, solutions)
T=length(test_covs);
n_competitors =length(solutions);

for j=1:n_competitors 
    subspaces=solutions{j}; 
    for i=1:T
        U_subspace=subspaces{i};
        score(i,j)=trace(U_subspace'*test_covs{i}*U_subspace)/trace(test_covs{i});
    end
end
end

